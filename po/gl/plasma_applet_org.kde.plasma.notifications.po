# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.es>, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012, 2013, 2014.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2019-08-19 20:26+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: fileinfo.cpp:170
#, kde-format
msgid "Open with %1"
msgstr ""

#: fileinfo.cpp:174
#, kde-format
msgid "Open with…"
msgstr ""

#: filemenu.cpp:115 package/contents/ui/JobItem.qml:267
#, kde-format
msgid "Open Containing Folder"
msgstr "Abrir o cartafol contedor"

#: filemenu.cpp:129
#, kde-format
msgid "&Copy"
msgstr "&Copiar"

#: filemenu.cpp:137
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr ""

#: filemenu.cpp:193
#, kde-format
msgid "Properties"
msgstr "Propiedades"

#: globalshortcuts.cpp:23
#, fuzzy, kde-format
#| msgid "Do not disturb"
msgid "Toggle do not disturb"
msgstr "Non molestar"

#: globalshortcuts.cpp:42
#, fuzzy, kde-format
#| msgid "Clear Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications Off"
msgstr "Retirar as notificacións"

#: globalshortcuts.cpp:43
#, fuzzy, kde-format
#| msgid "Clear Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications On"
msgstr "Retirar as notificacións"

#: package/contents/ui/EditContextMenu.qml:33
#, kde-format
msgid "Copy Link Address"
msgstr "Copiar o enderezo da ligazón"

#: package/contents/ui/EditContextMenu.qml:44
#, kde-format
msgid "Copy"
msgstr "Copiar"

#: package/contents/ui/EditContextMenu.qml:61
#, kde-format
msgid "Select All"
msgstr "Escoller todo"

#: package/contents/ui/FullRepresentation.qml:73
#, kde-format
msgid "Do not disturb"
msgstr "Non molestar"

#: package/contents/ui/FullRepresentation.qml:115
#, kde-format
msgid "For 1 hour"
msgstr "Durante 1 hora"

#: package/contents/ui/FullRepresentation.qml:120
#, kde-format
msgid "For 4 hours"
msgstr "Durante 4 horas"

#: package/contents/ui/FullRepresentation.qml:129
#, kde-format
msgid "Until this evening"
msgstr "Ata esta tarde"

#: package/contents/ui/FullRepresentation.qml:139
#, kde-format
msgid "Until tomorrow morning"
msgstr "Ata mañá pola mañá"

#: package/contents/ui/FullRepresentation.qml:151
#, kde-format
msgid "Until Monday"
msgstr "Ata o luns"

#: package/contents/ui/FullRepresentation.qml:158
#, kde-format
msgid "Until manually disabled"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:205
#, fuzzy, kde-format
#| msgid "Automatically hide"
msgctxt "Do not disturb until date"
msgid "Automatically ends: %1"
msgstr "Acochar automaticamente"

#: package/contents/ui/FullRepresentation.qml:217
#, kde-format
msgctxt "Do not disturb until app has finished (reason)"
msgid "While %1 is active (%2)"
msgstr "Mentres %1 estea activo (%2)"

#: package/contents/ui/FullRepresentation.qml:219
#, kde-format
msgctxt "Do not disturb until app has finished"
msgid "While %1 is active"
msgstr "Mentres %1 estea activo"

#: package/contents/ui/FullRepresentation.qml:225
#, kde-format
msgctxt "Do not disturb because external mirrored screens connected"
msgid "Screens are mirrored"
msgstr "As pantallas están reflectidas"

#: package/contents/ui/FullRepresentation.qml:401
#, kde-format
msgid "Close Group"
msgstr "Pechar o grupo"

#: package/contents/ui/FullRepresentation.qml:542
#, kde-format
msgid "Show Fewer"
msgstr "Mostrar menos"

#: package/contents/ui/FullRepresentation.qml:544
#, kde-format
msgctxt "Expand to show n more notifications"
msgid "Show %1 More"
msgstr "Mostrar %1 máis"

#: package/contents/ui/FullRepresentation.qml:584
#: package/contents/ui/main.qml:90
#, kde-format
msgid "No unread notifications"
msgstr "Non hai notificacións sen ler"

#: package/contents/ui/FullRepresentation.qml:599
#: package/contents/ui/main.qml:66
#, fuzzy, kde-format
#| msgid "Notifications and Jobs"
msgid "Notification service not available"
msgstr "Notificacións e tarefas"

#: package/contents/ui/FullRepresentation.qml:601
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2'"
msgstr ""

#: package/contents/ui/JobDetails.qml:35
#, kde-format
msgctxt "Row description, e.g. Source"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/JobDetails.qml:106
#, kde-format
msgctxt "How many bytes have been copied"
msgid "%2 of %1"
msgstr "%2 de %1"

#: package/contents/ui/JobDetails.qml:110
#, kde-format
msgctxt "How many files have been copied"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] "%2 de %1 ficheiro"
msgstr[1] "%2 de %1 ficheiros"

#: package/contents/ui/JobDetails.qml:113
#, kde-format
msgctxt "How many dirs have been copied"
msgid "%2 of %1 folder"
msgid_plural "%2 of %1 folders"
msgstr[0] "%2 de %1 cartafol"
msgstr[1] "%2 de %1 cartafoles"

#: package/contents/ui/JobDetails.qml:116
#, fuzzy, kde-format
#| msgctxt "How many files have been copied"
#| msgid "%2 of %1 file"
#| msgid_plural "%2 of %1 files"
msgctxt "How many items (that includes files and dirs) have been copied"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] "%2 de %1 ficheiro"
msgstr[1] "%2 de %1 ficheiros"

#: package/contents/ui/JobDetails.qml:124
#, kde-format
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] "%1 ficheiro"
msgstr[1] "%1 ficheiros"

#: package/contents/ui/JobDetails.qml:126
#, kde-format
msgid "%1 folder"
msgid_plural "%1 folders"
msgstr[0] "%1 cartafol"
msgstr[1] "%1 cartafoles"

#: package/contents/ui/JobDetails.qml:128
#, kde-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:145
#, kde-format
msgctxt "Bytes per second"
msgid "%1/s"
msgstr "%1/s"

#: package/contents/ui/JobItem.qml:163
#, fuzzy, kde-format
#| msgctxt "Row description, e.g. Source"
#| msgid "%1:"
msgctxt "Percentage of a job"
msgid "%1%"
msgstr "%1:"

#: package/contents/ui/JobItem.qml:177
#, kde-format
msgctxt "Pause running job"
msgid "Pause"
msgstr "Deter"

#: package/contents/ui/JobItem.qml:187
#, kde-format
msgctxt "Cancel running job"
msgid "Cancel"
msgstr "Cancelar"

#: package/contents/ui/JobItem.qml:198
#, kde-format
msgctxt "A button tooltip; hides item details"
msgid "Hide Details"
msgstr "Agochar os detalles"

#: package/contents/ui/JobItem.qml:199
#, kde-format
msgctxt "A button tooltip; expands the item to show details"
msgid "Show Details"
msgstr "Mostrar os detalles"

#: package/contents/ui/JobItem.qml:231
#: package/contents/ui/ThumbnailStrip.qml:169
#, fuzzy, kde-format
#| msgid "More Options..."
msgid "More Options…"
msgstr "Máis opcións…"

#: package/contents/ui/JobItem.qml:259
#, kde-format
msgid "Open"
msgstr "Abrir"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "%1 running job"
msgid_plural "%1 running jobs"
msgstr[0] "%1 tarefa en execución"
msgstr[1] "%1 tarefas en execución"

#: package/contents/ui/main.qml:54
#, kde-format
msgctxt "Job title (percentage)"
msgid "%1 (%2%)"
msgstr ""

#: package/contents/ui/main.qml:59
#, fuzzy, kde-format
#| msgctxt "Number of jobs and the speed at which they are downloading"
#| msgid "%1 running job (%2/s)"
#| msgid_plural "%1 running jobs (%2/s)"
msgid "%1 running job (%2%)"
msgid_plural "%1 running jobs (%2%)"
msgstr[0] "%1 tarefa en execución (%2/s)"
msgstr[1] "%1 tarefas en execución (%2/s)"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "%1 unread notification"
msgid_plural "%1 unread notifications"
msgstr[0] "%1 notificación sen ler"
msgstr[1] "%1 notificacións sen ler"

#: package/contents/ui/main.qml:83
#, fuzzy, kde-format
#| msgid "Do not disturb until %1"
msgid "Do not disturb until %1; middle-click to exit now"
msgstr "Non molestar ata o %1"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Do not disturb mode active; middle-click to exit"
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Middle-click to enter do not disturb mode"
msgstr ""

#: package/contents/ui/main.qml:193
#, fuzzy, kde-format
#| msgid "Clear Notifications"
msgid "Clear All Notifications"
msgstr "Retirar as notificacións"

#: package/contents/ui/main.qml:203
#, fuzzy, kde-format
#| msgid "&Configure Event Notifications and Actions..."
msgid "&Configure Event Notifications and Actions…"
msgstr "&Configurar as notificacións e accións para eventos…"

#: package/contents/ui/NotificationHeader.qml:122
#, fuzzy, kde-format
#| msgctxt "notification was just added, keep short"
#| msgid "Just now"
msgctxt "Notification was added less than a minute ago, keep short"
msgid "Just now"
msgstr "Hai un intre"

#: package/contents/ui/NotificationHeader.qml:128
#, kde-format
msgctxt "Notification was added minutes ago, keep short"
msgid "%1 min ago"
msgid_plural "%1 min ago"
msgstr[0] "Hai %1 min"
msgstr[1] "Hai %1 min"

#: package/contents/ui/NotificationHeader.qml:162
#, kde-format
msgctxt "seconds remaining, keep short"
msgid "%1 s remaining"
msgid_plural "%1 s remaining"
msgstr[0] "Resta %1 seg"
msgstr[1] "Restan %1 seg"

#: package/contents/ui/NotificationHeader.qml:166
#, kde-format
msgctxt "minutes remaining, keep short"
msgid "%1 min remaining"
msgid_plural "%1 min remaining"
msgstr[0] "Resta %1 min"
msgstr[1] "Restan %1 min"

#: package/contents/ui/NotificationHeader.qml:171
#, kde-format
msgctxt "hours remaining, keep short"
msgid "%1 h remaining"
msgid_plural "%1 h remaining"
msgstr[0] "Resta %1 hora"
msgstr[1] "Restan %1 horas"

#: package/contents/ui/NotificationHeader.qml:195
#, kde-format
msgid "Configure"
msgstr "Configurar"

#: package/contents/ui/NotificationHeader.qml:212
#, kde-format
msgctxt "Opposite of minimize"
msgid "Restore"
msgstr "Restaurar"

#: package/contents/ui/NotificationHeader.qml:213
#, kde-format
msgid "Minimize"
msgstr "Minimizar"

#: package/contents/ui/NotificationHeader.qml:236
#, kde-format
msgid "Close"
msgstr "Pechar"

#: package/contents/ui/NotificationItem.qml:183
#, kde-format
msgctxt "Job name, e.g. Copying is paused"
msgid "%1 (Paused)"
msgstr "%1 (detívose)"

#: package/contents/ui/NotificationItem.qml:188
#, kde-format
msgctxt "Job name, e.g. Copying has failed"
msgid "%1 (Failed)"
msgstr "%1 (fallou)"

#: package/contents/ui/NotificationItem.qml:190
#, kde-format
msgid "Job Failed"
msgstr "A tarefa fallou"

#: package/contents/ui/NotificationItem.qml:194
#, kde-format
msgctxt "Job name, e.g. Copying has finished"
msgid "%1 (Finished)"
msgstr "%1 (rematou)"

#: package/contents/ui/NotificationItem.qml:196
#, kde-format
msgid "Job Finished"
msgstr "Rematou a tarefa"

#: package/contents/ui/NotificationItem.qml:338
#, kde-format
msgctxt "Reply to message"
msgid "Reply"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:36
#, kde-format
msgctxt "Text field placeholder"
msgid "Type a reply…"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Send"
msgstr ""

#~ msgctxt "Do not disturb until date"
#~ msgid "Until %1"
#~ msgstr "Ata o %1"

#~ msgid "Clear History"
#~ msgstr "Borrar o historial"

#, fuzzy
#~| msgid "Select All"
#~ msgctxt "Clear all notifications"
#~ msgid "Clear All"
#~ msgstr "Escoller todo"

#~ msgctxt "Resume paused job"
#~ msgid "Resume"
#~ msgstr "Continuar"

#~ msgid "Until turned off"
#~ msgstr "Ata que o desactive"

#~ msgid "No unread notifications."
#~ msgstr "Non hai notificacións sen ler."

#~ msgid "Information"
#~ msgstr "Información"

#~ msgid "Show application and system notifications"
#~ msgstr "Mostrar notificacións de aplicativos e do sistema"

#~ msgid "Show a history of notifications"
#~ msgstr "Mostrar un historial de notificacións."

#~ msgid "Track file transfers and other jobs"
#~ msgstr "Seguir as transferencias de ficheiros e outras tarefas"

#~ msgid "Use custom position for the notification popup"
#~ msgstr "Usar unha posición personalizada para a xanela de notificacións."

#~ msgctxt "Either just 1 dir or m of n dirs are being processed"
#~ msgid "1 dir"
#~ msgid_plural "%2 of %1 dirs"
#~ msgstr[0] "1 directorio"
#~ msgstr[1] "%2 de %1 directorios"

#~ msgid "%1 of %2 %3"
#~ msgstr "%1 de %2 %3"

#~ msgid "No notifications or jobs"
#~ msgstr "Non hai ningunha notificación nin tarefa"

#~ msgctxt "10 seconds ago, keep short"
#~ msgid "10 s ago"
#~ msgstr "Hai 10 segundos"

#~ msgctxt "30 seconds ago, keep short"
#~ msgid "30 s ago"
#~ msgstr "Hai 30 segundos"

#~ msgctxt "notification was added yesterday, keep short"
#~ msgid "Yesterday"
#~ msgstr "Onte"

#~ msgctxt "notification was added n days ago, keep short"
#~ msgid "%1 day ago"
#~ msgid_plural "%1 days ago"
#~ msgstr[0] "Hai %1 día"
#~ msgstr[1] "Hai %1 días"

#~ msgid "History"
#~ msgstr "Historial"

#~ msgctxt ""
#~ "Indicator that there are more urls in the notification than previews shown"
#~ msgid "+%1"
#~ msgstr "+%1"

#~ msgctxt "the job, which can be anything, finished with error"
#~ msgid "%1: Error"
#~ msgstr "%1: Erro"

#~ msgid "Job Error"
#~ msgstr "Erro nunha tarefa"

#~ msgid "&Application notifications:"
#~ msgstr "&Notificacións dos programas:"

#~ msgid "&File transfers and jobs:"
#~ msgstr "Transferencias de &ficheiros e tarefas:"

#~ msgid "Choose which information to show"
#~ msgstr "Escolla a información para mostrar"

#~ msgid "Transfers"
#~ msgstr "Transferencias"

#~ msgid "All"
#~ msgstr "Todo"

#~ msgid "KiB/s"
#~ msgstr "KiB/s"

#~ msgid "%1 file, to: %2"
#~ msgid_plural "%1 files, to: %2"
#~ msgstr[0] "%1 ficheiro, a: %2"
#~ msgstr[1] "%1 ficheiros, a: %2"

#~ msgid "1 running job (%2 remaining)"
#~ msgid_plural "%1 running jobs (%2 remaining)"
#~ msgstr[0] "1 tarefa en execución (%2 restantes)"
#~ msgstr[1] "%1 tarefas en execución (%2 restantes)"

#~ msgid "%1 suspended job"
#~ msgid_plural "%1 suspended jobs"
#~ msgstr[0] "%1 tarefa suspendida"
#~ msgstr[1] "%1 tarefas suspendidas"

#~ msgid "%1 completed job"
#~ msgid_plural "%1 completed jobs"
#~ msgstr[0] "%1 tarefa completada"
#~ msgstr[1] "%1 tarefas completadas"

#~ msgctxt "Generic title for the job transfer popup"
#~ msgid "Jobs"
#~ msgstr "Tarefas"

#~ msgid "More"
#~ msgstr "Máis"

#~ msgid "Pause job"
#~ msgstr "Pausar a tarefa"

#~ msgctxt ""
#~ "%1 is the name of the job, can be things like Copying, deleting, moving"
#~ msgid "%1 [Finished]"
#~ msgstr "%1 [Rematado]"

#~ msgid "Less"
#~ msgstr "Menos"

#~ msgid "Notification from %1"
#~ msgstr "Notificación de %1"

#~ msgid "Pop Up Notices"
#~ msgstr "Mostrar os avisos"

#~ msgid "Popup"
#~ msgstr "Mostrar"
