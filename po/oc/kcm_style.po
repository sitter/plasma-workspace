# translation of kcmstyle.po to Occitan (lengadocian)
# Occitan translation of kcmstyle.po
#
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay.org> - 2006-2007
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcmstyle\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-02 00:47+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: gtkpage.cpp:59
#, kde-format
msgid "%1 is not a valid GTK Theme archive."
msgstr ""

#: kcmstyle.cpp:161 kcmstyle.cpp:168
#, kde-format
msgid "There was an error loading the configuration dialog for this style."
msgstr ""

#: kcmstyle.cpp:274
#, kde-format
msgid "Failed to apply selected style '%1'."
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:32
#, kde-format
msgid "Show icons:"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:33
#, fuzzy, kde-format
#| msgid "Button"
msgid "On buttons"
msgstr "Boton"

#: package/contents/ui/EffectSettingsPopup.qml:44
#, kde-format
msgid "In menus"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:56
#, kde-format
msgid "Main toolbar label:"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:58
#, kde-format
msgid "None"
msgstr ""

#: package/contents/ui/EffectSettingsPopup.qml:59
#, fuzzy, kde-format
#| msgid "Text Only"
msgid "Text only"
msgstr "Sonque tèxt"

#: package/contents/ui/EffectSettingsPopup.qml:60
#, fuzzy, kde-format
#| msgid "Text Alongside Icons"
msgid "Beside icons"
msgstr "Tèxt contra las icònas"

#: package/contents/ui/EffectSettingsPopup.qml:61
#, fuzzy, kde-format
#| msgid "Text Under Icons"
msgid "Below icon"
msgstr "Tèxt jos las icònas"

#: package/contents/ui/EffectSettingsPopup.qml:76
#, kde-format
msgid "Secondary toolbar label:"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:18
#, kde-format
msgid "GNOME/GTK Application Style"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:44
#, kde-format
msgid "GTK theme:"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:69
#, fuzzy, kde-format
#| msgid "Preview"
msgid "Preview…"
msgstr "Ulhada"

#: package/contents/ui/GtkStylePage.qml:88
#, kde-format
msgid "Install from File…"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:93
#, kde-format
msgid "Get New GNOME/GTK Application Styles…"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:109
#, kde-format
msgid "Select GTK Theme Archive"
msgstr ""

#: package/contents/ui/GtkStylePage.qml:111
#, kde-format
msgid "GTK Theme Archive (*.tar.xz *.tar.gz *.tar.bz2)"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgid ""
"This module allows you to modify the visual appearance of applications' user "
"interface elements."
msgstr ""

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Configure Style…"
msgstr ""

#: package/contents/ui/main.qml:116
#, kde-format
msgid "Configure Icons and Toolbars"
msgstr ""

#: package/contents/ui/main.qml:132
#, kde-format
msgid "Configure GNOME/GTK Application Style…"
msgstr ""

#: styleconfdialog.cpp:21
#, kde-format
msgid "Configure %1"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: stylepreview.ui:33
#, kde-format
msgid "Tab 1"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, pushButton)
#: stylepreview.ui:65
#, fuzzy, kde-format
#| msgid "Button"
msgid "Push Button"
msgstr "Boton"

#. i18n: ectx: property (text), item, widget (QComboBox, comboBox)
#: stylepreview.ui:86
#, kde-format
msgid "Combo box"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBox)
#: stylepreview.ui:96
#, kde-format
msgid "Checkbox"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, radioButton_2)
#. i18n: ectx: property (text), widget (QRadioButton, radioButton_1)
#: stylepreview.ui:106 stylepreview.ui:116
#, kde-format
msgid "Radio button"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: stylepreview.ui:133
#, kde-format
msgid "Tab 2"
msgstr ""

#. i18n: ectx: label, entry (widgetStyle), group (KDE)
#: stylesettings.kcfg:9
#, kde-format
msgid "Application style"
msgstr ""

#. i18n: ectx: label, entry (iconsOnButtons), group (KDE)
#: stylesettings.kcfg:13
#, kde-format
msgid "Show icons on buttons"
msgstr ""

#. i18n: ectx: label, entry (iconsInMenus), group (KDE)
#: stylesettings.kcfg:17
#, kde-format
msgid "Show icons in menus"
msgstr ""

#. i18n: ectx: label, entry (toolButtonStyle), group (Toolbar style)
#: stylesettings.kcfg:23
#, kde-format
msgid "Main toolbar label"
msgstr ""

#. i18n: ectx: label, entry (toolButtonStyleOtherToolbars), group (Toolbar style)
#: stylesettings.kcfg:27
#, kde-format
msgid "Secondary toolbar label"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Yannig Marchegay (Kokoyaya)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "yannig@marchegay.org"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "No description available."
#~ msgstr "Pas cap de descripcion disponibla."

#~ msgid "Group Box"
#~ msgstr "Quadre de grop"

#~ msgid "Icons Only"
#~ msgstr "Sonque icònas"

#, fuzzy
#~| msgid "&Style"
#~ msgctxt "@title:tab"
#~ msgid "&Style"
#~ msgstr "&Estil"
